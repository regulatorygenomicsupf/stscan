# stscan
#
# Seed Target SCAN is a tool for the discovery of target sites
#
# amadis.pages@upf.edu
# amadis.pages@crg.eu

use strict;

# Usage:
#   perl stscan.pl queryFile.fa tagetFile.fa seedSize targetSize numberMismatches

# ####################################################
# Dinucleotide scoring matrix
my %s_m = ();
# Watson-Crick pairs
$s_m{"TT"}{"AA"} =   93; $s_m{"AA"}{"TT"} =  93;
$s_m{"TA"}{"AT"} =  110; $s_m{"AT"}{"TA"} = 133;
$s_m{"GA"}{"CT"} =  208; $s_m{"TC"}{"AG"} = 208;
$s_m{"GT"}{"CA"} =  211; $s_m{"AC"}{"TG"} = 211;
$s_m{"CT"}{"GA"} =  235; $s_m{"AG"}{"TC"} = 235;
$s_m{"GC"}{"CG"} =  236; $s_m{"CG"}{"GC"} = 342;
$s_m{"CC"}{"GG"} =  326; $s_m{"GG"}{"CC"} = 326;
$s_m{"CA"}{"GT"} =  224; $s_m{"TG"}{"AC"} = 224;
# Wobbles
$s_m{"TT"}{"AG"} =   55; $s_m{"GA"}{"TT"} =  55;
$s_m{"TG"}{"AT"} =  136; $s_m{"TA"}{"GT"} = 136;
$s_m{"GT"}{"CG"} =  141; $s_m{"GC"}{"TG"} = 141;
$s_m{"GG"}{"CT"} =  211; $s_m{"TC"}{"GG"} = 211;
$s_m{"CT"}{"GT"} =  153; $s_m{"GG"}{"TC"} = 153;
$s_m{"CG"}{"GT"} =  251; $s_m{"TG"}{"GC"} = 251;
$s_m{"TT"}{"GA"} =  127; $s_m{"AG"}{"TT"} = 127;
$s_m{"TT"}{"GG"} =  -47; $s_m{"GG"}{"TT"} = -47;
$s_m{"TG"}{"GT"} = -129; $s_m{"GT"}{"TG"} = -30;
$s_m{"AT"}{"TG"} =  100; $s_m{"GT"}{"TA"} = 100;
# Mismatch
my $s_mm = -381;
# ####################################################

# Constants
my $tmp_d = $ENV{"PWD"};
my $query_tf = $tmp_d."/query_rc.fa";
my $target_tf = $tmp_d."/target_oc.fa";

# Parse command line
my ($query_f, $target_f, $seed_t, $target_t, $mm_t) = @ARGV;

# Global vars
my ($query_id, $query_seq);
my ($target_id, $target_seq);
my $sites;

# Check folders and files
die("Temporal folder $tmp_d is not writable") if (!(-d $tmp_d and -r $tmp_d));
die("Query file $query_f does not exist or is not readable") if (! -r $query_f);
die("Target file $target_f does not exist or is not readable") if (! -r $target_f);

# Parse query and target files
open(INPQ, " < $query_f");
while(!eof(INPQ)) {
  open(OUTQ, " > $query_tf");
  my $q_l = <INPQ>; chomp $q_l;
  $query_id = $q_l; $query_id =~ s/^>//g;
  print OUTQ "$q_l\n";
  $q_l = <INPQ>; chomp $q_l;
  $query_seq = reverse($q_l); $query_seq =~ tr/ACGT/TGCA/;
  print OUTQ "$query_seq\n";
  close(OUTQ);

  open(INPT, " < $target_f");
  while(!eof(INPT)) {
    open(OUTT, " > $target_tf");
    my $t_l = <INPT>; chomp $t_l;
    $target_id = $t_l; $target_id =~ s/^>//g;
    print OUTT "$t_l\n";
    $t_l = <INPT>; chomp $t_l;
    $target_seq = $t_l;
    print OUTT "$target_seq\n";
    close(OUTT);

    print STDERR "[LOG] Query = $query_id - Target = $target_id\n";

    print STDERR "[LOG]   Finding seeds\n";
    $sites = `/home/apages/development/stscan/stscan $query_tf $target_tf $seed_t 2> /dev/null`;
 
    print STDERR "[LOG]   Extending seeds\n";
    seed_and_extend() if ($sites !~ m/^$/);
  }
  close(INPT);
}
close(INPQ);
my $rmrst = `rm -f $target_tf $query_tf`;


# ################################################
# Function to extend seeds
# ################################################
sub seed_and_extend() {

  # Declare vars
  my %seeds_h = ();  # Hashtable of seeds
  my @query_a = ();  # Array of nucleotides for the query sequence
  my @target_a = (); # Array of nucleotides for the target sequence

  # Create array of query nucleotides
  @query_a = split //, $query_seq;
  @target_a = split //, $target_seq;
  unshift(@query_a, "X");
  unshift(@target_a, "X");

  # Store seeds in hash table
  my @seeds_a = split "\n", $sites;
  foreach my $seed (@seeds_a) {
    my ($q_sbp, $q_ebp, $t_sbp, $t_ebp) = split "\t", $seed;
    $seeds_h{$q_sbp}{$t_sbp}{"VISITED"} = 0;
    $seeds_h{$q_sbp}{$t_sbp}{"QEBP"} = $q_ebp;
    $seeds_h{$q_sbp}{$t_sbp}{"TEBP"} = $t_ebp;
  }

  # Visit seeds
  foreach my $seed (@seeds_a) {
    # Initialize values
    my $mm_counter = 0;
    my ($qsi, $qei, $tsi, $tei);

    # Extract coordinates
    my ($q_sbp, $q_ebp, $t_sbp, $t_ebp) = split "\t", $seed;
    ($qsi, $qei, $tsi, $tei) = ($q_sbp, $q_ebp, $t_sbp, $t_ebp);

    # Skip if seed has been visited
    next if ($seeds_h{$q_sbp}{$t_sbp}{"VISITED"} > 0);

    # First-right-then-left strategy
    # 1. Right
    while(($qei < scalar(@query_a)) and ($tei < scalar(@target_a)) and ($mm_counter <= $mm_t)) {
      $qei++; $tei++;
      # Matches a seed
      if (exists($seeds_h{$qei}{$tei})) {
        #$qei = $seeds_h{$q_sbp}{$t_sbp}{"QEBP"};
        #$tei = $seeds_h{$q_sbp}{$t_sbp}{"TEBP"};
        $seeds_h{$q_sbp}{$t_sbp}{"VISITED"}++;
      }
      # Match
      if ($query_a[$qei] eq $target_a[$tei]) {}
      # Wobble
      elsif (($query_a[$qei] eq "A") and ($target_a[$tei] eq "G")) {}
      elsif (($query_a[$qei] eq "C") and ($target_a[$tei] eq "T")) {}
      # Mismatch
      else {$mm_counter++}
    }
    if ($mm_counter > $mm_t) {
      $qei--; $tei--; $mm_counter--;
      my $stop = 0;
      while($stop < 1) {
        if ($query_a[$qei] eq $target_a[$tei]) { $stop++; }
        elsif (($query_a[$qei] eq "A") and ($target_a[$tei] eq "G")) { $stop++; }
        elsif (($query_a[$qei] eq "C") and ($target_a[$tei] eq "T")) { $stop++; }
        else { $qei--; $tei--; $mm_counter--; }
      }
    } 
    # 2. Left
    while(($qsi > 1) and ($tsi > 1) and ($mm_counter <= $mm_t)) {
      $qsi--; $tsi--;
      # Match
      if ($query_a[$qsi] eq $target_a[$tsi]) {}
      # Wobble
      elsif (($query_a[$qsi] eq "A") and ($target_a[$tsi] eq "G")) {}
      elsif (($query_a[$qsi] eq "C") and ($target_a[$tsi] eq "T")) {}
      # Mismatch
      else {$mm_counter++}
    }
    if ($mm_counter > $mm_t) {
      $qsi++; $tsi++;$mm_counter--;
      my $stop = 0;
      while($stop < 1) {
        if ($query_a[$qsi] eq $target_a[$tsi]) { $stop++; }
        elsif (($query_a[$qsi] eq "A") and ($target_a[$tsi] eq "G")) { $stop++; }
        elsif (($query_a[$qsi] eq "C") and ($target_a[$tsi] eq "T")) { $stop++; }
        else { $qsi++; $tsi++; $mm_counter--; }
      }
    }

    # Set binding site as visited
    $seeds_h{$q_sbp}{$t_sbp}{"VISITED"}++;

    # Print binding site
    if (($qei - $qsi + 1) >= $target_t) {

      my $binding = "";
      for (my $idx = 0; $idx < ($qei - $qsi + 1); $idx++) {
        if ($query_a[$qsi + $idx] eq $target_a[$tsi + $idx]) { $binding = $binding."|"; }
        elsif (($query_a[$qsi + $idx] eq "A") and ($target_a[$tsi + $idx] eq "G")) { $binding = $binding.":"; }
        elsif (($query_a[$qsi + $idx] eq "C") and ($target_a[$tsi + $idx] eq "T")) { $binding = $binding.":"; }
        else { $binding = $binding."."; }
      }

      my $score = score_binding($qsi, $qei, $tsi, $tei, $binding);
      
      print STDOUT "$query_id\t";
      print STDOUT length($query_seq) - $qei + 1, "\t";
      print STDOUT length($query_seq) - $qsi + 1, "\t";
      my $rev_p = join("", @query_a[$qsi..$qei]);
      $rev_p =~ tr/ACGT/TGCA/;
      $rev_p = reverse($rev_p);
      print STDOUT "$rev_p\t";
      print STDOUT "$target_id\t$tsi\t$tei\t", join("", @target_a[$tsi..$tei]), "\t";
      print STDOUT "$binding\t$score\n";
    }
  }
}


# ################################################
# Score binding site
# ################################################
sub score_binding() {
  my $qr_seq = $query_seq; $qr_seq =~ tr/ACGT/TGCA/;
  my ($qsi, $qei, $tsi, $tei, $binding) = @_;

  my $score = 0;
  my @query_a = split //, $qr_seq;
  my @target_a = split //, $target_seq;
  unshift(@query_a, "X");
  unshift(@target_a, "X");

  for (my $i = 0; $i < ($qei - $qsi); $i++) {
    my ($z, $k) = ($qsi+$i, $qsi+$i+1);
    my $q_di = join("", @query_a[$z..$k]);
    ($z, $k) = ($tsi+$i, $tsi+$i+1);
    my $t_di = join("", @target_a[$z..$k]);
    $score += $s_m{$q_di}{$t_di} if (exists($s_m{$q_di}{$t_di}));
    $score += $s_mm if (!exists($s_m{$q_di}{$t_di}));
    $i++ if (!exists($s_m{$q_di}{$t_di}));
  }

  return $score;
}

#include <lcut.h>
#include <fsm.h>
#include <kseq.h>
#include <sequence.h>
#include <stdio.h>
#include <zlib.h>

KSEQ_INIT(gzFile, gzread);

// FSM -> SAP MACRO test
void tc_fsm_sap (lcut_tc_t *tc, void *data)
{
  LCUT_INT_EQUAL(tc, 45, SAP(9));
  LCUT_INT_EQUAL(tc, -1, SAP(0));
  LCUT_INT_EQUAL(tc, -1, SAP(-9));
}

// FSM -> NTI MACRO test
void tc_fsm_nti (lcut_tc_t *tc, void *data)
{
  LCUT_INT_EQUAL(tc, 0, NTI('A'));
  LCUT_INT_EQUAL(tc, 1, NTI('C'));
  LCUT_INT_EQUAL(tc, 2, NTI('G'));
  LCUT_INT_EQUAL(tc, 3, NTI('T'));
  LCUT_INT_EQUAL(tc, -1, NTI('N'));
  LCUT_INT_EQUAL(tc, -1, NTI(8));
}

// FSM -> ITN MACRO test
void tc_fsm_itn (lcut_tc_t *tc, void *data) 
{
  LCUT_TRUE(tc, ITN(0) == 'A');
  LCUT_TRUE(tc, ITN(1) == 'C');
  LCUT_TRUE(tc, ITN(2) == 'G');
  LCUT_TRUE(tc, ITN(3) == 'T');
  LCUT_TRUE(tc, ITN(4) == 'N');
  LCUT_TRUE(tc, ITN('A') == 'N');
}

// FSM -> fsm_build test #1
void tc_fsm_fsm_build_1 (lcut_tc_t *tc, void *data) 
{
  fsm_struct fsm;
  char query[6] = {'A', 'T', 'G', 'T', 'C', '\0'};

  build_fsm(&fsm, query);

  LCUT_INT_EQUAL(tc, 14, fsm.total_states);
}

// FSM -> fsm_build test #2
void tc_fsm_fsm_build_2 (lcut_tc_t *tc, void *data)
{
  fsm_struct fsm;
  char query[6] = {'A', 'T', 'G', 'T', 'C', '\0'};

  build_fsm(&fsm, query);

  LCUT_STR_EQUAL(tc, "A", fsm.targets[1]);
  LCUT_STR_EQUAL(tc, "AT", fsm.targets[2]);
  LCUT_STR_EQUAL(tc, "ATG", fsm.targets[3]);
  LCUT_STR_EQUAL(tc, "ATGT", fsm.targets[4]);
  LCUT_STR_EQUAL(tc, "ATGTC", fsm.targets[5]);
  LCUT_STR_EQUAL(tc, "T", fsm.targets[6]);
  LCUT_STR_EQUAL(tc, "TG", fsm.targets[7]);
  LCUT_STR_EQUAL(tc, "TGT", fsm.targets[8]);
  LCUT_STR_EQUAL(tc, "TGTC", fsm.targets[9]);
  LCUT_STR_EQUAL(tc, "G", fsm.targets[10]);
  LCUT_STR_EQUAL(tc, "GT", fsm.targets[11]);
  LCUT_STR_EQUAL(tc, "GTC", fsm.targets[12]);
  LCUT_STR_EQUAL(tc, "TC", fsm.targets[13]);
  LCUT_STR_EQUAL(tc, "C", fsm.targets[14]);
}

// FSM -> fsm_build test #3
void tc_fsm_fsm_build_3 (lcut_tc_t *tc, void *data)
{
  fsm_struct fsm;
  char query[6] = {'A', 'T', 'G', 'T', 'C', '\0'};

  build_fsm(&fsm, query);

  LCUT_INT_EQUAL(tc, 1, fsm.states[0][NTI('A')]);  LCUT_INT_EQUAL(tc, 14, fsm.states[0][NTI('C')]);  LCUT_INT_EQUAL(tc, 10, fsm.states[0][NTI('G')]);  LCUT_INT_EQUAL(tc, 6, fsm.states[0][NTI('T')]);
  LCUT_INT_EQUAL(tc, 1, fsm.states[1][NTI('A')]);  LCUT_INT_EQUAL(tc, 14, fsm.states[1][NTI('C')]);  LCUT_INT_EQUAL(tc, 10, fsm.states[1][NTI('G')]);  LCUT_INT_EQUAL(tc, 2, fsm.states[1][NTI('T')]);
  LCUT_INT_EQUAL(tc, 1, fsm.states[2][NTI('A')]);  LCUT_INT_EQUAL(tc, 13, fsm.states[2][NTI('C')]);  LCUT_INT_EQUAL(tc, 3, fsm.states[2][NTI('G')]);   LCUT_INT_EQUAL(tc, 6, fsm.states[2][NTI('T')]);
  LCUT_INT_EQUAL(tc, 1, fsm.states[3][NTI('A')]);  LCUT_INT_EQUAL(tc, 14, fsm.states[3][NTI('C')]);  LCUT_INT_EQUAL(tc, 10, fsm.states[3][NTI('G')]);  LCUT_INT_EQUAL(tc, 4, fsm.states[3][NTI('T')]);
  LCUT_INT_EQUAL(tc, 1, fsm.states[4][NTI('A')]);  LCUT_INT_EQUAL(tc, 5, fsm.states[4][NTI('C')]);  LCUT_INT_EQUAL(tc, 7, fsm.states[4][NTI('G')]);    LCUT_INT_EQUAL(tc, 6, fsm.states[4][NTI('T')]);
  LCUT_INT_EQUAL(tc, 1, fsm.states[5][NTI('A')]);  LCUT_INT_EQUAL(tc, 14, fsm.states[5][NTI('C')]);  LCUT_INT_EQUAL(tc, 10, fsm.states[5][NTI('G')]);  LCUT_INT_EQUAL(tc, 6, fsm.states[5][NTI('T')]);
  LCUT_INT_EQUAL(tc, 1, fsm.states[6][NTI('A')]);  LCUT_INT_EQUAL(tc, 13, fsm.states[6][NTI('C')]);  LCUT_INT_EQUAL(tc, 7, fsm.states[6][NTI('G')]);   LCUT_INT_EQUAL(tc, 6, fsm.states[6][NTI('T')]);
  LCUT_INT_EQUAL(tc, 1, fsm.states[7][NTI('A')]);  LCUT_INT_EQUAL(tc, 14, fsm.states[7][NTI('C')]);  LCUT_INT_EQUAL(tc, 10, fsm.states[7][NTI('G')]);  LCUT_INT_EQUAL(tc, 8, fsm.states[7][NTI('T')]);
  LCUT_INT_EQUAL(tc, 1, fsm.states[8][NTI('A')]);  LCUT_INT_EQUAL(tc, 9, fsm.states[8][NTI('C')]);   LCUT_INT_EQUAL(tc, 7, fsm.states[8][NTI('G')]);   LCUT_INT_EQUAL(tc, 6, fsm.states[8][NTI('T')]);
  LCUT_INT_EQUAL(tc, 1, fsm.states[9][NTI('A')]);  LCUT_INT_EQUAL(tc, 14, fsm.states[9][NTI('C')]);  LCUT_INT_EQUAL(tc, 10, fsm.states[9][NTI('G')]);  LCUT_INT_EQUAL(tc, 6, fsm.states[9][NTI('T')]);
  LCUT_INT_EQUAL(tc, 1, fsm.states[10][NTI('A')]); LCUT_INT_EQUAL(tc, 14, fsm.states[10][NTI('C')]); LCUT_INT_EQUAL(tc, 10, fsm.states[10][NTI('G')]); LCUT_INT_EQUAL(tc, 11, fsm.states[10][NTI('T')]);
  LCUT_INT_EQUAL(tc, 1, fsm.states[11][NTI('A')]); LCUT_INT_EQUAL(tc, 12, fsm.states[11][NTI('C')]); LCUT_INT_EQUAL(tc, 7, fsm.states[11][NTI('G')]);  LCUT_INT_EQUAL(tc, 6, fsm.states[11][NTI('T')]);
  LCUT_INT_EQUAL(tc, 1, fsm.states[12][NTI('A')]); LCUT_INT_EQUAL(tc, 14, fsm.states[12][NTI('C')]); LCUT_INT_EQUAL(tc, 10, fsm.states[12][NTI('G')]); LCUT_INT_EQUAL(tc, 6, fsm.states[12][NTI('T')]);
  LCUT_INT_EQUAL(tc, 1, fsm.states[13][NTI('A')]); LCUT_INT_EQUAL(tc, 14, fsm.states[13][NTI('C')]); LCUT_INT_EQUAL(tc, 10, fsm.states[13][NTI('G')]); LCUT_INT_EQUAL(tc, 6, fsm.states[13][NTI('T')]);
  LCUT_INT_EQUAL(tc, 1, fsm.states[14][NTI('A')]); LCUT_INT_EQUAL(tc, 14, fsm.states[14][NTI('C')]); LCUT_INT_EQUAL(tc, 10, fsm.states[14][NTI('G')]); LCUT_INT_EQUAL(tc, 6, fsm.states[14][NTI('T')]);
}

// FSM -> fsm_build test #4
void tc_fsm_fsm_build_4 (lcut_tc_t *tc, void *data)
{
  fsm_struct fsm;
  char query[6] = {'A', 'T', 'G', 'T', 'C', '\0'};

  build_fsm(&fsm, query);

  LCUT_INT_EQUAL(tc, WRITE_GO, fsm.transitions[0][NTI('A')]);  LCUT_INT_EQUAL(tc, WRITE_GO, fsm.transitions[0][NTI('C')]);
  LCUT_INT_EQUAL(tc, WRITE_GO, fsm.transitions[0][NTI('G')]);  LCUT_INT_EQUAL(tc, WRITE_GO, fsm.transitions[0][NTI('T')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[1][NTI('A')]);  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[1][NTI('C')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[1][NTI('G')]);  LCUT_INT_EQUAL(tc, WRITE_GO, fsm.transitions[1][NTI('T')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[2][NTI('A')]);  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[2][NTI('C')]);
  LCUT_INT_EQUAL(tc, WRITE_GO, fsm.transitions[2][NTI('G')]);  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[2][NTI('T')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[3][NTI('A')]);  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[3][NTI('C')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[3][NTI('G')]);  LCUT_INT_EQUAL(tc, WRITE_GO, fsm.transitions[3][NTI('T')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[4][NTI('A')]);  LCUT_INT_EQUAL(tc, WRITE_GO, fsm.transitions[4][NTI('C')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[4][NTI('G')]);  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[4][NTI('T')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[5][NTI('A')]);  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[5][NTI('C')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[5][NTI('G')]);  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[5][NTI('T')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[6][NTI('A')]);  LCUT_INT_EQUAL(tc, WRITE_GO, fsm.transitions[6][NTI('C')]);
  LCUT_INT_EQUAL(tc, WRITE_GO, fsm.transitions[6][NTI('G')]);  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[6][NTI('T')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[7][NTI('A')]);  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[7][NTI('C')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[7][NTI('G')]);  LCUT_INT_EQUAL(tc, WRITE_GO, fsm.transitions[7][NTI('T')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[8][NTI('A')]);  LCUT_INT_EQUAL(tc, WRITE_GO, fsm.transitions[8][NTI('C')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[8][NTI('G')]);  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[8][NTI('T')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[9][NTI('A')]);  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[9][NTI('C')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[9][NTI('G')]);  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[9][NTI('T')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[10][NTI('A')]); LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[10][NTI('C')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[10][NTI('G')]); LCUT_INT_EQUAL(tc, WRITE_GO, fsm.transitions[10][NTI('T')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[11][NTI('A')]); LCUT_INT_EQUAL(tc, WRITE_GO, fsm.transitions[11][NTI('C')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[11][NTI('G')]); LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[11][NTI('T')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[12][NTI('A')]); LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[12][NTI('C')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[12][NTI('G')]); LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[12][NTI('T')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[13][NTI('A')]); LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[13][NTI('C')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[13][NTI('G')]); LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[13][NTI('T')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[14][NTI('A')]); LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[14][NTI('C')]);
  LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[14][NTI('G')]); LCUT_INT_EQUAL(tc, WRITE_CL, fsm.transitions[14][NTI('T')]);
}

// KSEQ -> complete test
void tc_kseq_all (lcut_tc_t *tc, void *data)
{
  char* names[132];
  char* sequences[132];
  gzFile fp; 
  kseq_t *seq; 
  int l; 
  int idx = 0;

  fp = gzopen("test/dat/query.fa", "r");
  seq = kseq_init(fp); 
  while ((l = kseq_read(seq)) >= 0) { 
    names[idx] = (char*) malloc((strlen(seq->name.s) + 1) * sizeof(char));
    strncpy(names[idx], seq->name.s, strlen(seq->name.s));
    sequences[idx] = (char*) malloc((strlen(seq->seq.s) + 1) * sizeof(char));
    strncpy(sequences[idx], seq->seq.s, strlen(seq->seq.s));
    idx++;
  }

  kseq_destroy(seq); 
  gzclose(fp);

  LCUT_STR_EQUAL(tc, "ENSG00000207421|ENST00000384690", names[89]);
  LCUT_STR_EQUAL(tc, "ACTCACTGATGAGTAGCTTCTGACTTTCATTCTGAGTTTGCTGAACCCAGATGCCATTCCTGGGAAGG", sequences[123]);
}

// SEQUENCE -> NC MACRO test
void tc_seq_cn (lcut_tc_t *tc, void *data)
{
  LCUT_TRUE(tc, CN('A') == 'T');
  LCUT_TRUE(tc, CN('C') == 'G');
  LCUT_TRUE(tc, CN('G') == 'C');
  LCUT_TRUE(tc, CN('T') == 'A');
  LCUT_TRUE(tc, CN('I') == 'N');
  LCUT_TRUE(tc, CN(9) == 'N');
}

// SEQUENCE -> reverse test
void tc_seq_sqreverse (lcut_tc_t *tc, void *data)
{
  char sequence[11] = {'A', 'C', 'T', 'T', 'T', 'G', 'A', 'A', 'A', 'G', '\0'};
  char rev[11];

  sqreverse(rev, sequence);

  LCUT_STR_EQUAL(tc, "GAAAGTTTCA", rev);
}

void tc_seq_sqcomplement (lcut_tc_t *tc, void *data)
{
  char sequence[11] = {'A', 'C', 'T', 'T', 'T', 'G', 'A', 'A', 'A', 'G', '\0'};
  char rev[11];

  sqcomplement(rev, sequence);

  LCUT_STR_EQUAL(tc, "TGAAACTTTC", rev);
}

void tc_seq_sqrevcomp (lcut_tc_t *tc, void *data)
{
  char sequence[11] = {'A', 'C', 'T', 'T', 'T', 'G', 'A', 'A', 'A', 'G', '\0'};
  char rev[11];

  sqrevcomp(rev, sequence);

  LCUT_STR_EQUAL(tc, "CTTTCAAAGT", rev);
}

int main(void)
{
  lcut_ts_t *suite = NULL;

  LCUT_TEST_BEGIN("UNIT TEST", NULL, NULL);

  // FSM suite
  LCUT_TS_INIT(suite, "FSM", NULL, NULL);
  LCUT_TC_ADD(suite, "SAP MACRO test", tc_fsm_sap, NULL, NULL, NULL);
  LCUT_TC_ADD(suite, "NTI MACRO test", tc_fsm_nti, NULL, NULL, NULL);
  LCUT_TC_ADD(suite, "ITN MACRO test", tc_fsm_itn, NULL, NULL, NULL);
  LCUT_TC_ADD(suite, "fsm_build test 1", tc_fsm_fsm_build_1, NULL, NULL, NULL);
  LCUT_TC_ADD(suite, "fsm_build test 2", tc_fsm_fsm_build_2, NULL, NULL, NULL);
  LCUT_TC_ADD(suite, "fsm_build test 3", tc_fsm_fsm_build_3, NULL, NULL, NULL);
  LCUT_TC_ADD(suite, "fsm_build test 4", tc_fsm_fsm_build_4, NULL, NULL, NULL);
  LCUT_TS_ADD(suite);

  // KSEQ suite
  LCUT_TS_INIT(suite, "KSEQ", NULL, NULL);
  LCUT_TC_ADD(suite, "Complete test", tc_kseq_all, NULL, NULL, NULL);
  LCUT_TS_ADD(suite);

  // SEQUENCE suite
  LCUT_TS_INIT(suite, "SEQUENCE", NULL, NULL);
  LCUT_TC_ADD(suite, "NC MACRO test", tc_seq_cn, NULL, NULL, NULL);
  LCUT_TC_ADD(suite, "reverse test", tc_seq_sqreverse, NULL, NULL, NULL);
  LCUT_TC_ADD(suite, "complement test", tc_seq_sqcomplement, NULL, NULL, NULL);
  LCUT_TC_ADD(suite, "revcomp test", tc_seq_sqrevcomp, NULL, NULL, NULL);
  LCUT_TS_ADD(suite);

  LCUT_TEST_RUN();
  LCUT_TEST_REPORT();
  LCUT_TEST_END();
  LCUT_TEST_RESULT();

  return(0);
}

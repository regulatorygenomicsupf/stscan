#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <fsm.h>
#include <kseq.h>

void ptest_build_fsm (FILE *output)
{ 
  struct timeval t0, t1;
  unsigned int i, j;
  fsm_struct fsm;

  for (i = 100; i <= 1500; i += 100) {
    char* query = (char*) malloc((i + 1) * sizeof(char));
    for (j = 0; j < i; j++) {
      int r = rand();
      query[j] = ITN(r % 4);
    }
    query[i] = '\0';

    gettimeofday(&t0, NULL);
    build_fsm(&fsm, query);
    gettimeofday(&t1, NULL);

    fprintf(output, "build_fsm\tTIME\t%u\t%.2g\n", i, t1.tv_sec - t0.tv_sec + 1E-6 * (t1.tv_usec - t0.tv_usec));
    fprintf(output, "build_fsm\tMEMORY\t%u\t%lu\n", i, fsm.total_memory);
    fflush(output);

    gettimeofday(&t0, NULL);
    destroy_fsm(&fsm, query);
    gettimeofday(&t1, NULL);

    fprintf(output, "destroy_fsm\tTIME\t%u\t%.2g\n", i, t1.tv_sec - t0.tv_sec + 1E-6 * (t1.tv_usec - t0.tv_usec));
    fflush(output);

    free(query);
  }
}

int main(int argc, char** argv)
{
  FILE *output = fopen(argv[1], "w");

  // FSM performance test
  ptest_build_fsm(output);

  fclose(output);

  return (0);
}

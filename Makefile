CC = gcc
OBJS = build/sequence.o build/fsm.o
FLAGS = -Wall -c

utest: fsm.o sequence.o utest.o
	gcc -o utest -L/home/apages/tools/lcut-0.3.0/lib/ $(OBJS) build/utest.o -llcut -lz
	./utest

ptest: fsm.o sequence.o ptest.o
	gcc -o ptest $(OBJS) build/ptest.o -lz
	./ptest test/report/ptest.tab
	/soft/R/R-3.1.0/bin/R --vanilla --silent < test/src/plot.R > /dev/null

utest.o: src/include/fsm.h src/include/kseq.h src/include/sequence.h
	$(CC) $(FLAGS) test/src/utest.c -Isrc/include/ -I/home/apages/tools/lcut-0.3.0/include/ -o build/utest.o

ptest.o: src/include/fsm.h src/include/kseq.h src/include/sequence.h
	$(CC) $(FLAGS) test/src/ptest.c -Isrc/include/ -o build/ptest.o

stscan: fsm.o sequence.o stscan.o
	gcc -o stscan $(OBJS) build/stscan.o -lz

stscan.o : src/include/fsm.h src/include/kseq.h src/include/sequence.h
	$(CC) $(FLAGS) src/stscan.c -Isrc/include -o build/stscan.o

fsm.o: src/include/fsm.h
	$(CC) $(FLAGS) src/fsm.c -Isrc/include -o build/fsm.o

sequence.o : src/include/sequence.h
	$(CC) $(FLAGS) src/sequence.c -Isrc/include -o build/sequence.o

clean:
	rm -rf build/*
	rm -rf test/report/*
	rm -f utest
	rm -f ptest
	rm -f stscan

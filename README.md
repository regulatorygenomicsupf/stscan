# STScan

STScan is a tool for the prediction of RNA:RNA interactions

## Getting started

### Prerequisites

Git - [http://git-scm.com/](http://git-scm.com/)

Perl - [https://www.perl.org/get.html](https://www.perl.org/get.html)

### Clone serpent-analysis

```
git clone https://amadis_pages@bitbucket.org/regulatorygenomicsupf/stscan.git
cd stscan
```

### Execute stscan

```
perl stscan.pl query_file target_file seed_length interaction_site_length number_mismatches
```
where:
```
seed_length : minimum length required for the seed
interaction_site_length : minimum length required for the interaction site
number_mismatches : maximum number of mismatches allowed in the interaction site
```
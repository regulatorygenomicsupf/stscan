/*
 * Complement Nucleotide
 * Return the complementary nucleotide of the given nucleotide
 *   A <-> T, G <-> C
 */
#define CN(N) (N == 'A' ? 'T' : (N == 'C' ? 'G' : (N == 'G' ? 'C' : (N == 'T' ? 'A' : 'N'))))

/*
 * Reverse given sequence
 */
void sqreverse (char* rev_seq, char* sequence);

/*
 * Complement given sequence
 */
void sqcomplement (char* comp_seq, char* sequence);

/*
 * Reverse-complement sequence
 */
void sqrevcomp (char* rc_seq, char* sequence);

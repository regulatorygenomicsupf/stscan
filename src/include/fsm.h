/*
 * Number of nucleotides
 */
#define NUCS 4

/*
 * FSM transitions
 */
#define WRITE_GO 1
#define WRITE_CL 2

/*
 * Sum of Arithmetic Progression
 * Sum of the terms of an arithmetic progression with n terms where:
 *   a_{1} = 1
 *   a_{n} = a_{n-1} + 1
 */
#define SAP(A) (A <= 0 ? -1 : (A * (1 + A)) / 2)

/*
 * Nucleotide To Index and Index To Nucleotide
 * Return an index that varies according the input nucleotide
 *   A <-> 0, C <-> 1, G <-> 2, T <-> 3
 */
#define NTI(N) (N == 'A' ? 0 : (N == 'C' ? 1 : (N == 'G' ? 2 : (N == 'T' ? 3 : -1))))
#define ITN(I) (I == 0 ? 'A' : (I == 1 ? 'C' : (I == 2 ? 'G' : (I == 3 ? 'T' : 'N'))))

/*
 * Struct for handling FSM
 */
typedef struct {
  int **states;
  char **targets;
  int **transitions;
  int total_states;
  unsigned long total_memory;
} fsm_struct;

/*
 * Builds a FSM that recognizes all the possible substrings of a query string
 */
void build_fsm(fsm_struct *fsm, char* query);

/*
 * Frees allocated memory of a FSM
 */
void destroy_fsm(fsm_struct *fsm, char *query);

/*
 * Parses a given sequence with a FSM and recognizes all the possible substrings of a minimum length
 */
void parse_sequence(fsm_struct *fsm, char *query, char *sequence, int length);

#include <sequence.h>
#include <stdlib.h>
#include <string.h>

/*
 * reverse
 */
void sqreverse (char* rev_seq, char* sequence)
{
  int idx;

  for(idx = 0; idx < strlen(sequence); idx++)
    rev_seq[idx] = sequence[strlen(sequence) - 1 - idx];
  rev_seq[idx] = '\0';
}

/*
 * complement
 */
void sqcomplement (char* comp_seq, char* sequence)
{
  int idx;

  for(idx = 0; idx < strlen(sequence); idx++) 
    comp_seq[idx] = CN(sequence[idx]);
  comp_seq[idx] = '\0';
}

/*
 * revcomp
 */
void sqrevcomp (char* rc_seq, char* sequence)
{
  int idx;

  for(idx = 0; idx < strlen(sequence); idx++) 
    rc_seq[idx] = CN(sequence[strlen(sequence) - 1 - idx]);
  rc_seq[idx] = '\0';
}

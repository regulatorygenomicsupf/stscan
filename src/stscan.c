#include <fsm.h>
#include <sequence.h>
#include <kseq.h>
#include <zlib.h>
#include <stdio.h>

KSEQ_INIT(gzFile, gzread);

/*
 * Application entry point
 */
int main(int argc, char **argv)
{
  gzFile query_f;
  kseq_t *query_s;
  int query_l, length;
  fsm_struct fsm;

  // Parse minimum length
  length = atoi(argv[3]);

  // Open query file
  query_f = gzopen(argv[1], "r");
  query_s = kseq_init(query_f);
 
  while ((query_l = kseq_read(query_s)) >= 0) {
    int db_l;
    gzFile db_f =  gzopen(argv[2], "r");
    kseq_t *db_s = kseq_init(db_f);

    // Build FSM
    fprintf(stderr, "[LOG] Building FSM for query %s\n", query_s->name.s);
    build_fsm(&fsm, query_s->seq.s);

    // Parse genes
    while ((db_l = kseq_read(db_s)) >= 0) {
    fprintf(stderr, "[LOG]   Parsing sequence %s\n", db_s->name.s);
      parse_sequence(&fsm, query_s->seq.s, db_s->seq.s, length);
    }
 
    kseq_destroy(db_s);
    gzclose(db_f);
  }

  kseq_destroy(query_s);
  gzclose(query_f);

  return (0);
}

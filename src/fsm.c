#include <fsm.h>
#include <sequence.h>
#include <string.h>
#include <stdlib.h>

#include <stdio.h>

/*
 * Auxiliar function for building FSM
 */
void parse_reverse_fsm(fsm_struct* rfsm, char* new_target, char* old_target)
{
  char* sequence = (char*) malloc((strlen(old_target) + 1) * sizeof(char));
  char* curr_target = (char*) malloc((strlen(old_target) + 1) * sizeof(char));
  int idx;
  int curr_state = 0;
  int char_idx = 0;
  char nuc;

  sqreverse(sequence, old_target);

  idx = 0;
  nuc = sequence[idx];
  while (rfsm->states[curr_state][NTI(nuc)] >= 0) {
    curr_state  = rfsm->states[curr_state][NTI(nuc)];
    curr_target[char_idx++] = nuc;
    idx++;
    nuc = sequence[idx];  
  }
  curr_target[char_idx] = '\0';

  sqreverse(new_target, curr_target);

  free(sequence);
  free(curr_target);
}

/*
 * Auxiliar function for building FSM
 */
int parse_fsm(fsm_struct* fsm, char* target)
{
  int curr_state = 0;
  int idx;

  for (idx = 0; idx < strlen(target); idx++) {
    char nuc = target[idx];
    curr_state  = fsm->states[curr_state][NTI(nuc)];
  }

  return (curr_state);
}

/*
 *  build_fsm
 */
void build_fsm(fsm_struct *fsm, char* query)
{
  int ouidx, inidx;                 // Outer and inner indexes
  int qlen = strlen(query);         // Length of the query string
  fsm_struct reverse;               // Reverse FSM
  int maxstates = SAP(qlen) + 1;    // Maximum number of states of the forward and reverse FSMs
  char* reverse_query;              // Reversed query

  // First step - build forward FSM
  fsm->total_memory = sizeof(fsm_struct);
  fsm->states = (int**) malloc(maxstates * sizeof(int*));
  fsm->total_memory += maxstates * sizeof(int*);
  fsm->targets = (char**) malloc(maxstates * sizeof(char*));
  fsm->total_memory += maxstates * sizeof(char*);
  fsm->transitions = (int**) malloc(maxstates * sizeof(int*));
  fsm->total_memory += maxstates * sizeof(int*);
  fsm->total_states = 0;

  for(ouidx = 0; ouidx < maxstates; ouidx++) {
    fsm->states[ouidx] = (int*) malloc(NUCS * sizeof(int));
    fsm->total_memory += NUCS * sizeof(int);
    fsm->transitions[ouidx] = (int*) malloc(NUCS * sizeof(int));
    fsm->total_memory += NUCS * sizeof(int);
    for (inidx = 0; inidx < NUCS; inidx++) fsm->states[ouidx][inidx] = -1;
  }

  for (ouidx = 0; ouidx < qlen; ouidx++) {
    int curr_state = 0;
    char *curr_target = (char*) malloc((qlen + 1) * sizeof(char));
    int curr_target_idx = 0;

    for (inidx = ouidx; inidx < qlen; inidx++) {
      char nuc = query[inidx];
      curr_target[curr_target_idx++] = nuc;

      if (fsm->states[curr_state][NTI(nuc)] < 0) {
        fsm->total_states++;
        fsm->states[curr_state][NTI(nuc)] = fsm->total_states;
        fsm->transitions[curr_state][NTI(nuc)] = WRITE_GO;
      }

      curr_state = fsm->states[curr_state][NTI(nuc)];
      curr_target[curr_target_idx] = '\0';
      if (curr_state != 0) {
        fsm->targets[curr_state] = (char*) malloc((strlen(curr_target) + 1) * sizeof(char));
        fsm->total_memory += (strlen(curr_target) + 1) * sizeof(char);
        strncpy(fsm->targets[curr_state], curr_target, strlen(curr_target) + 1);
      }
    }

    free(curr_target);
  }

  // Second step - build reverse FSM
  reverse_query = (char*) malloc(qlen * sizeof(char));
  sqreverse(reverse_query, query);

  reverse.states = (int**) malloc(maxstates * sizeof(int*));
  reverse.total_states = 0;

  for(ouidx = 0; ouidx < maxstates; ouidx++) {
    reverse.states[ouidx] = (int*) malloc(NUCS * sizeof(int));
    for (inidx = 0; inidx < NUCS; inidx++) reverse.states[ouidx][inidx] = -1;
  }

  for (ouidx = 0; ouidx < qlen; ouidx++) {
    int curr_state = 0;

    for (inidx = ouidx; inidx < qlen; inidx++) {
      char nuc = reverse_query[inidx];
      if (reverse.states[curr_state][NTI(nuc)] < 0) {
        reverse.total_states++;
        reverse.states[curr_state][NTI(nuc)] = reverse.total_states;
      }
      curr_state = reverse.states[curr_state][NTI(nuc)];
    }
  }

  free(reverse_query);

  // Third step - refine FSM
  for (ouidx = 1; ouidx <= fsm->total_states; ouidx++) {
    for (inidx = 0; inidx < NUCS; inidx++) {
      if (fsm->states[ouidx][inidx] < 0) {
        int curr_t_len = strlen(fsm->targets[ouidx]);
        char* curr_target = (char*) malloc((curr_t_len + 2) * sizeof(char));
        strncpy(curr_target, fsm->targets[ouidx], curr_t_len);
        curr_target[curr_t_len] = ITN(inidx);
        curr_target[curr_t_len + 1] = '\0';

        // Find longest substring using the reverse FSM
        char* new_target = (char*) malloc((curr_t_len + 2) * sizeof(char));
        parse_reverse_fsm(&reverse, new_target, curr_target);

        // Find and store new state
        int new_state = parse_fsm(fsm, new_target);
        fsm->states[ouidx][inidx] = new_state;
        fsm->transitions[ouidx][inidx] = WRITE_CL;

        free(curr_target);
        free(new_target);
      }
    }
  }

  // Free reverse FSM
  for (ouidx = 0; ouidx < maxstates; ouidx++)
    free(reverse.states[ouidx]);
  free(reverse.states);
}

/*
 * destroy_fsm
 */
void destroy_fsm(fsm_struct *fsm, char *query)
{
  int ouidx;
  int maxstates = SAP(strlen(query)) + 1;

  for (ouidx = 0; ouidx < maxstates; ouidx++) {
    free(fsm->states[ouidx]);
    if ((ouidx > 0) && (ouidx <= fsm->total_states))
      free(fsm->targets[ouidx]);
    free(fsm->transitions[ouidx]);
  }

  free(fsm->states);
  free(fsm->targets);
  free(fsm->transitions);
}

/*
 * parse_sequence
 */
void parse_sequence(fsm_struct *fsm, char *query, char *sequence, int length)
{
  int curr_state = 0;
  unsigned long char_idx;

  for (char_idx = 0; char_idx < strlen(sequence); char_idx++) {
    char nuc = sequence[char_idx];

    if ((fsm->transitions[curr_state][NTI(nuc)] == WRITE_CL) && (strlen(fsm->targets[curr_state]) >= length)) {
      unsigned long query_sbp = strstr(query, fsm->targets[curr_state]) - query;
      unsigned long query_ebp = query_sbp + strlen(fsm->targets[curr_state]) - 1;
      unsigned long db_sbp = char_idx - strlen(fsm->targets[curr_state]) + 1; // char_index is pointing to the nucleotide next to the last nucleotide of the target
      unsigned long db_ebp = char_idx;                                        // char_index is pointing to the nucleotide next to the last nucleotide of the target
fprintf(stdout, "%lu\t%lu\t%lu\t%lu\n", query_sbp + 1, query_ebp + 1, db_sbp, db_ebp);
    }

    curr_state  = fsm->states[curr_state][NTI(nuc)];
  }
}
